DESCRIPTION:

This Program solves the 1-dimensional Schroedinger-Equotation.

Data is going to be read from "schroedinger.inp", so here you have to fill in 
the Data of Problem that should be solved.

The Results are written in Files:
    discrpot.dat -- The discretised Potential V(x)
    energies.dat -- The Eigenvalues/Energies of the given Problem
    wfuncs.dat   -- The x-Values in the first column, the requested wavefunctions in the following columns
    ewfuncs.dat  -- The Wavefunctions shifted with the Eigenvalues

This program expects an inputfile called "schroedinger.inp" containing the needed input by the user.
There are a few Examples of how the "schroedinger.inp" has to look like in "/autotest".
____________________________________________________________________________________________________________
USAGE:

First of all you have to compile the program using:

      	$ make

make sure a schroedinger.inp input-file is contained within this folder, then to run this program use:

	$ ./schroedinger

To preform a test whether this program is functioning as intended use:

	$ make test

To remove files created by this program after it finished it's tasks either use:

	$ make clean

or for a complete cleanup also deleting the 'schroedinger' and 'auto_test' binary files use:

	$ make realclean

however after this step the program needs to be compiled again (see the first command).



