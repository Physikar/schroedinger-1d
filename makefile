FC = gfortran
FCOPTS = -llapack -pedantic -Wall -fbounds-check
LN = $(FC)
LNOPTS = -llapack
BORDERTEST=""

OBJS = accuracy.o io.o calc.o schroedinger.o
A_OBJS = accuracy.o io.o autotest.o

.PHONY: all
all: schroedinger auto_test

schroedinger: $(OBJS)
	$(LN) -o $@ $^ $(LNOPTS)

auto_test: $(A_OBJS)
	$(LN) -o $@ $^

%.o: src/%.f90
	$(FC) -c $< $(FCOPTS)

accuracy.o:

calc.o: accuracy.o

io.o: accuracy.o

autotest.o: accuracy.o io.o

schroedinger.o: accuracy.o io.o calc.o


.PHONY: clean realclean test

clean:
	rm -f *.o *.mod *.dat && rm -fr ./autotest/results

realclean: clean
	rm -f schroedinger auto_test

test:
	./autotest/autotest.sh
