module io
  use accuracy
  implicit none
  
  contains

    !> Reads the information from data file
    !!
    !! \param inputfile Character name of inputdata-file
    !! \param mass Real double precision mass of the particle on exit
    !! \param xMin Real double precision lower limit of Wavefunction on exit
    !! \param xMax Real double precision upper limit of Wavefunction on exit
    !! \param nPoint Integer number of iterations on exit
    !! \param type Character interpolationtype should be linear, polynomial, cspline on exit
    !! \param intpolPs Matrix fixpoints of Potential for Interpolation
    !! \param fEigenv Integer first Eigenvalue, that will be saved on exit
    !! \param lEigenv Integer last Eigenvalue, that will be saved on exit
    !! \param nIntpolPs Integer number of assembling points on exit
    !!
    !! \note If one of the assembling points has the same x-value like the next one then there could appear a division by 0.  
    subroutine readinput(inputfile, mass, xMin, xMax, nPoint, type, intpolPs, fEigenv, lEigenv, nIntpolPs)

      character(len=*), intent(in) :: inputfile
      character(10), intent(out) :: type
      real(dp), allocatable, intent(out) :: intpolPs(:,:)
      real(dp), intent(out) :: mass, xMin, xMax
      integer, intent(out) :: fEigenv, lEigenv, nPoint, nIntpolPs

      !help variables
      integer, parameter :: fd = 12
      integer :: ii, open_status

      !open of File
      open(fd, file=inputfile, status="old", iostat=open_status)
    if (open_status /= 0) then
      write (*, *) "Error: readinput: File cannot be opened!"
      stop
    end if

      !read of File-Data
      read(fd,*) mass
      read(fd,*) xMin, xMax, nPoint
      read(fd,*) fEigenv, lEigenv
      read(fd,*) type
      read(fd,*) nIntpolPs

      !read Interpolationpoints linewise
      allocate(intpolPs(nIntpolPs,2))

      do ii = 1, nIntpolPs
         read(fd,*) intpolPs(ii,:)
      end do

      close(12)

    end subroutine readinput

    
    !> Writes the calculated results to Files.
    
    !! \param xx Vector contains the x-values where should be interpolate
    !! \param pot Vector contains y-values of the Potential
    !! \param energies Vector the calculated Energie-eigenvalues
    !! \param wavef Matrix wavefunction, as a 2-dimensional array (x, y).
    !! \param fEigenv Integer first Eigenvalue, that will be saved
    !! \param lEigenv Integer last Eigenvalue, that will be saved
    subroutine write_output (xx, pot, energies, wavef, fEigenv, lEigenv)
      real(dp), intent(in) :: xx(:), pot(:), energies(:), wavef(:,:)
      integer, intent(in) :: fEigenv, lEigenv

      integer :: ii
      character (len = 256) :: write_string

      !Validationcheck of given data
      if ((fEigenv >= lEigenv) .or. (fEigenv < 0) .or. (lEigenv < 0) .or. &
      & (size (xx) /= size(pot)) .or. (size (energies) /= size (wavef, 2)) .or. &
      & (size (xx) /= size(wavef, 2)) .or. (lEigenv > size(energies))) then
         write(*,*) "Error: write_output: Data invaild, please check!"
         stop
      end if

      !Opening of needed Files
      open (14, file="discrpot.dat", status="replace", form="formatted")
      open (15, file="energies.dat", status="replace", form="formatted")
      open (16, file="wfuncs.dat", status="replace", form="formatted")
      open (17, file="ewfuncs.dat", status="replace", form="formatted")

      !Write the Potential
      do ii=1, size(pot)
         write (14, "(ES23.15, 1X, ES23.15)") xx(ii), pot(ii)
      end do
      close(14)

      !Write the eigenvalues (energies).
      write (15, "(ES23.15)") energies (fEigenv:lEigenv)
      close (15)

      !Create the writestring
      write(write_string, "(A,I0,A)") "(", (lEigenv - fEigenv + 2), "(ES23.15, X))"

      !Write the Wavefunction
      do ii=1, size (xx)
         write(16, write_string) xx(ii), wavef(ii, fEigenv:lEigenv)
         write(17, write_string) xx(ii), wavef(ii, fEigenv:lEigenv) + energies (fEigenv:lEigenv)
      end do
      close (16)
      close (17)
    end subroutine write_output

    !> Reading the Eigenvalues of the Solution
    
    !! \param fName Character Name of the Solutionfile
    !! \param ev Real double precision Vector with Solution

    subroutine read_solution(fName, ev)
      character(len=*), intent(in) :: fName
      real(dp), intent(out) :: ev(:)

      integer, parameter :: fd = 21

      open(fd, file=fname, status="old", action="read", form="formatted")
      read(fd, *) ev
      close(fd)
    end subroutine read_solution

end module io
