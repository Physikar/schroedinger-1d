!> Contains the routines to interpolate the potential and solve the 
!! Schrödinger equation in 1 dimension
module calc
  use accuracy

  implicit none

contains

  !> Calculate the Potential with linear interpolation
  !!
  !! \param potx Vector contains x-values of the assembling points
  !! \param poty Vector contains y-values of the assembling points
  !! \param xx Vector contains equidistant x-values where should be interpolated
  !! \param VV Vector contains calculated potential on exit. Same dimension as xx.
  subroutine interpolate_linear(potx, poty, xx, VV)
    real(dp), intent(in) :: xx(:), potx(:), poty(:)
    real(dp), intent(inout) :: VV(:)

    integer :: ii, jj
    real(dp) :: number

    do ii = 1, size(xx(:))
       if (size(potx(:)) == 2) then
          !!Potential on the left side of the potential well
          if (xx(ii) < potx(1)) then
             VV(ii) = huge(number)
             !!Potential on the right side of the potential well
          elseif (xx(ii) > potx(size(potx(:)))) then
             VV(ii) = huge(number)
             !!Potential of the potential well
          elseif (potx(1) /= potx(2)) then
             !!Use the gradient between two given points and the 
             !!distance to the left given point
             VV(ii) = (poty(2)-poty(1))/ &
                 & (potx(2)-potx(1))* &
                 & (xx(ii)-potx(1))+poty(1)
          elseif (abs(poty(1)) < abs(poty(2))) then
             VV(ii) = poty(2)
          else
             VV(ii) = poty(2)
          end if
       else
          !!Potential on the left side of the potential well
          if (xx(ii) <= potx(1)) then
             VV(ii) = poty(1)
             !!Potential on the right side of the potential well
          elseif (xx(ii) >= potx(size(potx(:)))) then
             VV(ii) = poty(size(potx(:)))
             !!Potential of the potential well
          else
             do jj = 2, size(potx(:)) - 1
                !!Use given points if x values are equal
                if (xx(ii) == potx(jj)) then
                   !!Skip the second given Point if two have the same x value
                   if (potx(jj) /= potx(jj-1)) then
                      !!Use the higher absolut y value if two of the given
                      !!points have the same x values
                      if (potx(jj) == potx(jj+1)) then
                         if (abs(poty(jj)) > abs(poty(jj+1))) then
                            VV(ii) = poty(jj)
                         else
                            VV(ii) = poty(jj+1)
                         end if
                         !!Use the given Point
                      else
                         VV(ii) = poty(jj)
                      end if
                   end if
                   !!Potential between two given points
                else
                   if (((potx(1) < xx(ii)) .and. &
                       &  (xx(ii) < potx(2)))) then
                      !!Use the gradient between two given points and the distance
                      !!to the left given point
                      VV(ii) = (poty(2)-poty(1))/ &
                          & (potx(2)-potx(1))* &
                          & (xx(ii)-potx(1))+poty(1)
                   elseif (((potx(jj) < xx(ii)) .and. &
                       &  (xx(ii) < potx(jj+1)))) then
                      !!Use the gradient between two given points and the distance
                      !!to the left given point
                      VV(ii) = (poty(jj+1)-poty(jj))/ &
                          & (potx(jj+1)-potx(jj))* &
                          & (xx(ii)-potx(jj))+poty(jj)
                   end if
                end if
             end do
          end if
       end if
    end do
  end subroutine interpolate_linear

  !> Calculate potential with polynomial interpolation
  !!
  !! \param potx Vector contains x-values of the assembling points
  !! \param poty Vector contains y-values of the assembling points
  !! \param xx Vector contains equidistant x-values where should be interpolated
  !! \param nPoint number Integer number of points that should be calculated
  !! \param VV Vector contains calculated potential on exit. Same dimension as xx.
  subroutine interpolate_polynomial (potx, poty, xx, nPoint, VV)
    real(dp), intent (in) :: potx(:), poty(:), xx(:)
    integer, intent (in) :: nPoint
    real(dp), intent (inout) :: VV(:)

    real(dp) :: vValue
    real(dp), allocatable :: polyArr(:,:), pivot(:), polynomial(:)
    integer cx, cy, retValue, ii, jj

    if ( size (potx) == size (poty) ) then
       allocate (polynomial(size(potx)))
       allocate (polyArr (size (potx), size (poty)))
       polynomial = poty

       !!Fill polynomial matrix
       do cx = 1, size (potx)
          do cy = 1, size(poty)
             if (cx == 1) then
                polyArr(cy, cx) = 1
             else
                polyArr(cy, cx) = potx (cy)**(cx - 1)
             end if
          end do
       end do

       !!Solving the LES with LAPACK
       allocate (pivot (size(potx)))
       call DGESV (size(potx), 1, polyArr, size(potx), pivot, polynomial, size(polynomial), retValue)
    else
       write(*,*) "Error: Creation of Polynomial: You need as many x values as y values, please correct!"
       stop
    end if

    !Calculation of the Polynomial for x
    do ii = 1, nPoint  
       !Set the first coefficient of the polynomial for x^0
       vValue = polynomial(1)
       do jj = 1, size (polynomial) - 1
          vValue = vValue + polynomial (jj + 1) * (xx(ii) ** jj)
       end do
       VV(ii) = vValue
    end do

  end subroutine interpolate_polynomial

  !> Subroutine using cubic spline interpolation for approximation of the potential.
  !!
  !! \details The implementation of this routine was done on the basis of
  !!  "http://www.arndt-bruenner.de/mathe/scripts/kubspline.htm".
  !!  Furthermore this subroutine depends on the DGTSV subroutine contained in
  !!  LAPACK for solving a system of linear equations. For more information
  !!  about LAPACK see "http://www.netlib.org/lapack/".
  !!
  !! \param potx The vector containing the x-coordinates of the potential sampling points.
  !! \param poty The vector containing the y-coordinates  of the potential sampling points.
  !! \param xx The vector containing the  equidistant x-coordinates of the
  !!  potential to interpolate.
  !! \param VV The vector containing the y-coordinates of the interpolated
  !!  potential on exit.
  !!
  !! \note This subroutine can't handle multiple assigned f(x)-values for the
  !!  same x-value, meaning all values within potx must differ.
  !!  This subroutine gives out a message, if the DGTSV succeeded solving the
  !!  system of linear equations or not.
  !!  
  subroutine interpolate_cspline (potx, poty, xx, VV)
    real(dp), intent (in) :: potx(:), poty(:), xx(:)
    real(dp), intent (inout) :: VV(:)

    real(dp), allocatable :: diag(:), subdiag(:), superdiag(:), rhs(:,:)
    real(dp), allocatable :: aa(:), bb(:), cc(:), dd(:)
    integer :: info, ii, ldim, nrhs

    allocate(diag(size(potx)-2))
    allocate(subdiag(size(potx)-3))
    allocate(superdiag(size(potx)-3))
    allocate(rhs(size(potx)-2,1))
    allocate(aa(size(potx)-1))
    allocate(bb(size(potx)))
    allocate(cc(size(potx)-1))
    allocate(dd(size(potx)))

    !! calculating the coefficients dd_i
    do ii = 1, size(poty)
       dd(ii) = poty(ii)
    end do

    !! calculating the rhs and the diag of the tridiagonalmatrix
    do ii = 1, size(potx)-2
       diag(ii) = 2*(potx(ii+2) - potx(ii))
       rhs(ii,1) = 3*((dd(ii+2) - dd(ii+1)) / (potx(ii+2) - potx(ii+1))&
           & - (dd(ii+1) - dd(ii)) / (potx(ii+1) - potx(ii)))
    end do

    !! creating the super- and subdiagonals of the tridiagonalmatrix
    do ii = 1, size(potx)-3
       subdiag(ii) = potx(ii+2) - potx(ii+1)
       superdiag(ii) = subdiag(ii)
    end do

    !! defining ldim of the tridiagonalmatrix and the number of rhs
    ldim = size(potx)-2
    nrhs = 1
    !! solving linear system and calculating the bb_i for 1 < i < ldim via DGTSV
    call DGTSV (ldim, nrhs, subdiag, diag, superdiag, rhs, ldim, info)

    !! checking if the DGTSV routine worked properly
    if (info == 0) then
       write(*,*) "The DGTSV routine  worked properly."
    else
       write(*,*) "The DGTSV routine had an error."
    end if

    !! defining fixed coefficients bb_i
    bb(1) = 0
    bb(size(potx)) = 0
    !! calculating nonefixed coefficients bb_i
    do ii = 1, size(potx)-2
       bb(ii+1)= rhs(ii,1)
    end do

    !! calculating the coefficiens aa_i and ac_i with already calculated b_i
    do ii = 1, size(potx) -1
       aa(ii) = (bb(ii+1) - bb(ii)) / (3*(potx(ii+1) - potx(ii)))
       cc(ii) = ((dd(ii+1) - dd(ii))/(potx(ii+1) - potx(ii)))&
           & - ((bb(ii+1) - bb(ii))*(potx(ii+1) - potx(ii))/3) &
           & - (bb(ii)*(potx(ii+1) - potx(ii)))
    end do

    !! calculating VV with calculated, aa_i, bb_i, cc_i and d_i via interpolation         
    do ii = 1, size(potx)-1
       where (potx(ii)<= xx .and. xx < potx(ii+1))
          VV = aa(ii) * (xx - potx(ii))**3 &
              & + bb(ii) * (xx - potx(ii))**2 &
              & + cc(ii) * (xx - potx(ii)) + dd(ii)
       end where
    end do

  end subroutine interpolate_cspline

  !> solves the Eigenvalue Matrix
  !! 
  !! \param mass Real doubleprecision mass of the object in the potential
  !! \param nPoint Integer number Integer number of points that should be calculated
  !! \param delta Real doubleprecision distant between interpolation points
  !! \param VV Vector contains calculated potential
  !! \param energies Vector filled with Eigenvalues
  !! \param wavef Matrix filled with wavefunctions in one column and x-values in the other
  !! \param info Integer if info = 0 then LES solved without errors on exit

  subroutine mat_solver(mass, nPoint, delta, VV, energies, wavef, info)
    real(dp), intent(in) :: mass, delta, VV(:)
    integer, intent(in) :: nPoint

    real(dp), allocatable, intent(out) :: energies(:), wavef(:,:)
    integer, intent(out) :: info

    real(dp) :: aa
    real(dp), allocatable :: vec(:), vec_2(:,:)

    !allocate the vectors energies and vec with Order nPoint
    allocate(energies(nPoint))
    allocate(vec(nPoint))
    allocate(wavef(nPoint,nPoint))
    allocate(vec_2(2*nPoint,2*nPoint))

    !calculate factor a
    aa = (1 / (mass * delta * delta))

    !write diagonal elements in Vector energies
    !(in output the eigenvalues ascending)
    energies(:) = aa + VV(:)

    !write nPoint - 1 elements in Vector vec
    vec(:) = -0.5 * aa

    !Destruction of last element (if necessary)
    vec(nPoint) = 0

    !solve with algorithm from LAPACK
    call DSTEV ('V', nPoint, energies, vec, wavef, nPoint, vec_2, info)

  end subroutine mat_solver


end module calc
