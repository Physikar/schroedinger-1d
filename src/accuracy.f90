!> Contains accuracy related settings
!!
module accuracy
  implicit none

  !> kind for double precision
  integer, parameter :: dp = selected_real_kind(15,300)

end module accuracy
