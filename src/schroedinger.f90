!> Mainprogram to solve the Schroedinger Equation
!!
!! the Program imports data from inputdata, export it to modul calc where the data would be interpolated linear, polynomial or with splines and solve the LES of the Schroedinger Equation. Finally the wanted solutions would be saved in outputfiles. 
!! 

program schroedinger
  use accuracy
  use io
  use calc

  implicit none

  character(*), parameter :: inputfile = "schroedinger.inp"

  real(dp), allocatable :: intpolPs(:,:), xx(:), VV(:), energies(:), wavef(:,:)
  real(dp) :: mass, xMin, xMax, delta
  integer :: nPoint, fEigenv, lEigenv, info, ii, nintpolPs
  character(10) :: type

  !import data from schroedinger.inp
  write(*,"(3A)") "read input file '", inputfile, "'"
  call readinput(inputfile, mass, xMin, xMax, nPoint, type, intpolPs, fEigenv, lEigenv, nintpolPs)
  write(*,*) "reading completed"

  !calculating the Step-Size
  delta = (xMax - xMin)/(nPoint - 1)

  !Creation of all x Values for Potential and Wavefunction
  allocate(xx(nPoint))
  do ii = 0, (nPoint - 1)
     xx(ii+1) = (ii)*delta + xMin
  end do

  !allocation of the Potential
  allocate(VV(nPoint))

  write(*,*) "Data conversion complete"
  write(*,*) "Interpolationtype: ", type
  
  !Check which Interpolation required and do Interpolation
  if(type == "linear") then
        call interpolate_linear (intpolPs(:,1), intpolPs(:,2), xx, VV)
  elseif(type == "polynomial") then
        call interpolate_polynomial (intpolPs(:,1), intpolPs(:,2), xx, nPoint, VV)
  elseif(type == "cspline") then
        call interpolate_cspline (intpolPs(:,1), intpolPs(:,2), xx, VV)
  else
     write(*,*) "Wrong Interpolation Type entered, please correct type"
     stop
  end if

!!Calculation of Eigenvalues
write(*,*) "Solving the Eigenvalue Matrix"
call mat_solver(mass, nPoint, delta, VV, energies, wavef, info)

 if(info == 0)then
     write(*,*) 'Matrix successfully solved without Errors'
  elseif(info < 0)then
     write(*,*) 'Error: illegal Value for Matrixelement', info
     stop
  elseif(info > 0)then
     write(*,*) 'Error, not convergent Value for Matrixelement', info
     stop
  end if

!Write results to files

call write_output (xx, VV, energies, wavef, fEigenv, lEigenv)

end program schroedinger
