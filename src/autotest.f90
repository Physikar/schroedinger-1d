!! this Program tests two Resulting files for Compatibility

program autotest
  use accuracy
  use io
  implicit none

  character(28), dimension(5), parameter :: origname = (/"autotest/energies_1.dat.orig", & 
  &"autotest/energies_2.dat.orig", "autotest/energies_3.dat.orig", "autotest/energies_4.dat.orig", &
  &"autotest/energies_5.dat.orig"/)

  character(31), dimension(5), parameter :: newname = (/"autotest/results/energies_1.dat", &
  &"autotest/results/energies_2.dat", "autotest/results/energies_3.dat", "autotest/results/energies_4.dat", &
  &"autotest/results/energies_5.dat"/)

  character(len=*), parameter :: testok = "TEST PASSED"
  character(len=*), parameter :: testfailed = "!!! TEST FAILED !!!"
  real(dp), parameter :: errortol = 1e-10_dp
  integer :: ii
  real(dp), dimension(15, 5) :: orig(15, 5), new(15,5)

do ii=1,5
   call read_solution(origname(ii), orig(:,ii))
   call read_solution(newname(ii), new(:,ii))
   if (maxval(abs(new(:,ii) - orig(:,ii))) >= errortol) then
      write(*,"(A,1X,A)") testfailed, "Data shapes are different"
   elseif (maxval(abs(new(:,ii) - orig(:,ii))) <= errortol) then
      write(*,"(A,I2,1X,A)") "test #", ii, testok
   else
      write(*,"(A,1X,A)") testfailed, "Another Problem"
   end if
end do

end program autotest
